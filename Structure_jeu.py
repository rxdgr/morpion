# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 17:43:41 2019

@author: rxdgr
"""

import Affichage_et_saisie as AS
import Structure_jeu as SJ

class Joueur:
    def __init__(self, nom, symbole):
        self.nom = nom
        self.symbole = symbole

class Jeu:
    def __init__(self, dimension):
        self.joueurs = []
        self.grille = []
        self.dimension = dimension
        
        for x in range(0, dimension):
            self.grille.append([])
            
            for y in range (0, dimension):
                self.grille[x].append(-1)
    
    def nom_du_joueur(self):
        name = input("Joueur 1, quel est ton nom ? ")
        symbole = input("Choisis ton symbole : ")
        self.joueurs.append(Joueur(name, symbole))
        name = input("Joueur 2, quel est ton nom ? ")
        symbole = input("Choisis ton symbole : ")
        self.joueurs.append(Joueur(name, symbole))
        




