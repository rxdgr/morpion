# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 17:45:03 2019

@author: rxdgr
"""

import Test_condition_de_victoire as TV
import Affichage_et_saisie as AS
import Structure_jeu as SJ

def partie_morpion(Jeu):
    while True :
        AS.affichage(Jeu)
        print("C'est le tour de",Jeu.joueurs[0].nom,"\b.","\n")
        AS.jouer_un_coup(Jeu, 0)
        AS.affichage(Jeu)
        TV.bilan_du_tour(Jeu, 0)
        if TV.joueur_gagne(Jeu, 0) == True:
            break
        if TV.match_nul(Jeu) == True:
            break
        print("C'est le tour de",Jeu.joueurs[1].nom,"\b.","\n")
        AS.jouer_un_coup(Jeu, 1)
        AS.affichage(Jeu)
        TV.bilan_du_tour(Jeu, 1)
        if TV.joueur_gagne(Jeu, 1) == True:
            break
        if TV.match_nul(Jeu) == True:
            break
