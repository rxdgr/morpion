# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 17:44:35 2019

@author: rxdgr
"""

import Affichage_et_saisie as AS
import Structure_jeu as SJ

def joueur_gagne_une_ligne(Jeu, numero_joueur, numero_ligne):
    liste = []
    for n in Jeu.grille[numero_ligne]:
        if n == numero_joueur:
            liste.append(Jeu.grille[numero_ligne])
    if len(liste) == Jeu.dimension:
        return True
    else:
        return False

def joueur_gagne_lignes(Jeu, numero_joueur):
    for n in range (0, Jeu.dimension):
        joueur_gagne_une_ligne (Jeu, numero_joueur, n)
        if joueur_gagne_une_ligne (Jeu, numero_joueur, n):
            return True

def joueur_gagne_une_colonne(Jeu, numero_joueur, numero_colonne):
    liste = []
    for n in range (0, Jeu.dimension):
        if Jeu.grille[n][numero_colonne] == numero_joueur:
            liste.append(Jeu.grille[n][numero_colonne])
    if len(liste) == Jeu.dimension:
       return True
    else :
       return False

def joueur_gagne_colonnes(Jeu, numero_joueur):
    for n in range (0, Jeu.dimension):
        joueur_gagne_une_colonne (Jeu, numero_joueur, n)
        if joueur_gagne_une_colonne (Jeu, numero_joueur, n):
            return True

def joueur_gagne_diagonale_gauche(Jeu, numero_joueur):
    liste = []
    gauche = Jeu.dimension - 1
    for n in range (0, Jeu.dimension):
        if Jeu.grille[gauche][gauche] == numero_joueur:
            liste.append(Jeu.grille[gauche][gauche])
        gauche = gauche - 1
    if len(liste) == Jeu.dimension:
        return True
    else:
        return False
        
        
def joueur_gagne_diagonale_droite(Jeu, numero_joueur):
    liste = []
    ligne = Jeu.dimension - 1
    colonne = 0
    for n in range (0, Jeu.dimension):
        if Jeu.grille[ligne][colonne] == numero_joueur:
            liste.append(Jeu.grille[ligne][colonne])
        ligne = ligne - 1
        colonne = colonne + 1
    if len(liste) == Jeu.dimension:
        return True
    else:
        return False


def match_nul(Jeu):
    for elements in Jeu.grille:
        for n in range (0, Jeu.dimension):
            for x in range (0, Jeu.dimension):
                if Jeu.grille[n][x] == -1 :
                    return False
    return True

def joueur_gagne(Jeu, numero_joueur):
    if joueur_gagne_lignes(Jeu, numero_joueur):
        return True
    elif joueur_gagne_colonnes(Jeu, numero_joueur):
        return True
    elif joueur_gagne_diagonale_droite(Jeu, numero_joueur):
        return True
    elif joueur_gagne_diagonale_gauche(Jeu, numero_joueur):
        return True
    else:
        return False

def bilan_du_tour(Jeu, numero_joueur):
    if joueur_gagne(Jeu, numero_joueur) == True:
        print("Victoire ! Bravo",Jeu.joueurs[numero_joueur].nom, "\b, tu as gagné.")
    elif match_nul(Jeu) == True :
        print("Match nul!")
    else:
        print("Essayez encore!\n")

		
    
    
    
