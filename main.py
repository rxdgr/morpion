# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 17:43:02 2019

@author: rxdgr
"""

import Partie_morpion as PM
import Test_condition_de_victoire as TV
import Affichage_et_saisie as AS
import Structure_jeu as SJ



def main():
    
    dimension = int(input("Entrez la dimension souhaitée pour le jeu : "))
    mon_jeu = SJ.Jeu(dimension)
    mon_jeu.nom_du_joueur()
    PM.partie_morpion(mon_jeu)
    
main()
