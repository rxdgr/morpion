# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 17:44:07 2019

@author: rxdgr
"""

import Structure_jeu as SJ


def conversion(Jeu, valeur):
    if valeur == -1 :
        return " "
    elif valeur == 0:
        return Jeu.joueurs[0].symbole
    elif valeur == 1 :
        return Jeu.joueurs[1].symbole
           

def affichage(Jeu):
    
    for l in range(0, Jeu.dimension):
        print("+--"*Jeu.dimension,"\b+")
        for c in range (0, Jeu.dimension):
            
            print("|",conversion(Jeu,Jeu.grille[l][c]),end="")            
        print("|")
    print("+--"*Jeu.dimension,"\b+")


def saisir_valeur(Jeu):
    valeur = 0
    print("Tapez une valeur entre 1 et", Jeu.dimension,":")
    while valeur<1 or valeur>Jeu.dimension:
        valeur = int(input())
    return valeur

def saisir_coordonnees(Jeu):
    print("Entrez tout d'abord le numero de la ligne.\n")
    l = saisir_valeur(Jeu)
    print("Entrez maintenant le numero de la colonne.\n")
    c = saisir_valeur(Jeu)
    return l,c

def case_est_libre(Jeu, nb_ligne, nb_colonne):
    if Jeu.grille [nb_ligne-1][nb_colonne-1] == -1:
        return True
    else:
        print("Malheureusement, cette case est deja jouée ! \nSaisissez une autre case svp ! \n")
        return False

def jouer_un_coup(Jeu, numero_joueur):
    l, c = saisir_coordonnees(Jeu)
    x = case_est_libre (Jeu, l, c)
    while x == False :
        l, c = saisir_coordonnees(Jeu)
        x = case_est_libre (Jeu, l, c)
    Jeu.grille[l-1][c-1] = numero_joueur
    
    

